const Sqlite = require("better-sqlite3");

class BadORM {
  _db;
  _tableName;
  stmts = {};

  constructor(dbPath, tableName, tableDef = {}) {
    this._db = new Sqlite(dbpath);
    this.db.pragma("synchronous = 1");
    this.db.pragma("journal_mode = wal");
    this._tableName = tableName;

    if (!BadORM.doesTableExist(tableName)) {
      let fields = [];
      Object.keys(tableDef).forEach((key) =>
        fields.push(`${key} ${tableDef[key]}`)
      );
      const sql = `CREATE TABLE ${tableName} (${tableFields.join(", ")});`;
      this.db.prepare(sql).run();
    }

    //assuming tables will always have an `id` field, cf class name
    this.prepareStatement("get", `SELECT * FROM ${tableName} WHERE id = ?`);
    this.prepareStatement("delete", `DELETE * FROM ${tableName} WHERE id = ?`);
  }

  prepareStatement(name, sql) {
    this.stmts[name] = this.db.prepare(sql);
  }

  static doesTableExist(database, tableName) {
    const sql = `SELECT count(*) FROM sqlite_master WHERE type='table' AND name = '${tableName}';`;
    const table = database.prepare(sql).get();
    return Boolean(table["count(*)"]);
  }

  get(id) {
    this.stmts.get.run(id);
  }

  delete(id) {
    this.stmts.delete.run(id);
  }
}
