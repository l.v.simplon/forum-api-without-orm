const config = require("./config.js");
const DbMgr = require("./TopicManager.js");
const crud = new DbMgr(config.dbPath);

const router = require("express").Router();

router.post("/newtopic", (req, res) => {
  const { title, content, author } = req.body;

  const dbResTopic = crud.storeTopic({ title });
  threadId = dbResTopic.lastInsertRowid;
  console.log("dbResTopic", dbResTopic, threadId);
  const dbRes = crud.storePost({
    topic_id: threadId,
    author,
    content,
  });
  console.log("store res:", dbRes);

  res.json({ threadId });
});

router.post("/reply", (req, res) => {
  const { threadId, content, author } = req.body;

  const dbRes = crud.storePost({
    topic_id: threadId,
    author,
    content,
  });
  console.log("store reply res:", dbRes);

  res.json({ postId: dbRes.lastInsertRowid });
});

router.get("/topic", (req, res) => {
  const { id } = req.body;
  const dbRes = crud.getTopics(id);
  console.log("topic listing result:", dbRes);
  res.json({ result: dbRes });
});

router.delete("/post/:id", (req, res) => {
  const { id } = req.params;
  console.log("deleting post", id);
  const dbRes = crud.deletePost(id);
  console.log("post deletion result:", dbRes);
  res.json({ result: dbRes });
});

router.delete("/topic/:id", (req, res) => {
  const { id } = req.params;
  console.log("deleting topic", id);
  const dbRes = crud.deleteTopic(id);
  console.log("topic deletion result:", dbRes);
  res.json({ result: dbRes });
});

router.get("/getAll", (req, res) => {
  const dbRes = crud.getPostsAndTopics();
  console.log(dbRes);
  res.json({ Posts: dbRes });
});

router.get("/replies/:threadId", getPaginated);
router.get("/replies/:threadId/:page", getPaginated);

function getPaginated(req, res) {
  const NBPERPAGE = 50;
  const threadId = req.params.threadId;
  const page = req.params.page || 0;

  const records = crud.getPosts(threadId, page * NBPERPAGE, NBPERPAGE);
  const total = crud.getCount(threadId);

  const returnData = {
    total: total["COUNT()"],
    offset: page * NBPERPAGE,
    resPerPage: NBPERPAGE,
    records,
  };

  console.log(returnData);
  res.json(returnData);
}

module.exports = router;
