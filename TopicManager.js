const DbMgr = require("./DbMgr.js");

const dbTables = [
  {
    tableName: "topics",
    tableFields: [
      "id INTEGER PRIMARY KEY AUTOINCREMENT",
      "title TEXT NOT NULL",
    ],
  },
  {
    tableName: "posts",
    tableFields: [
      "id INTEGER PRIMARY KEY AUTOINCREMENT",
      "topic_id INTEGER NOT NULL",
      "content TEXT NOT NULL",
      "author TEXT NOT NULL",
      "date DATE DEFAULT (datetime('now'))",
      "FOREIGN KEY (topic_id) REFERENCES topics(id) ON DELETE CASCADE",
    ],
  },
];

const dbQueries = [
  {
    name: "storeTopic",
    sql: `INSERT INTO topics (title) VALUES (
			:title
		);`,
  },
  {
    name: "storePost",
    sql: `INSERT INTO posts (topic_id, content, author) VALUES (
			:topic_id,
			:content,
			:author
		);`,
  },
  {
    name: "getPostsPaged",
    sql:
      "SELECT * FROM posts WHERE topic_id = ? ORDER BY datetime(date) ASC LIMIT ? OFFSET ?;",
  },
  {
    name: "getCount",
    sql: "SELECT COUNT() FROM posts WHERE topic_id = ?;",
  },
  {
    name: "getTopics",
    sql: "SELECT * FROM topics;",
  },
  {
    name: "getPostsAndTopics",
    sql:
      "SELECT * FROM topics LEFT JOIN posts ON topics.id = posts.topic_id ORDER BY datetime(posts.date) ASC;",
  },
  {
    name: "deletePost",
    sql: "DELETE FROM posts WHERE id = ?;",
  },
  {
    name: "deleteTopic",
    sql: "DELETE FROM topics WHERE id = ?;",
  },
];

class Db {
  dbMgr = null;

  constructor(path) {
    this.dbMgr = new DbMgr(path, dbTables, dbQueries);
  }

  storeTopic(data) {
    console.log("storeTopic data:", data);
    return this.dbMgr.stmts.storeTopic.run(data);
  }

  storePost(data) {
    console.log("storePost data:", data);
    return this.dbMgr.stmts.storePost.run(data);
  }

  getPosts(topicId, offset = 0, limit = 10) {
    const data = this.dbMgr.stmts.getPostsPaged.all(topicId, limit, offset);

    console.log("getPosts", data);
    return data;
  }

  deletePost(id) {
    return this.dbMgr.stmts.deletePost.run(id);
  }

  deleteTopic(id) {
    return this.dbMgr.stmts.deleteTopic.run(id);
  }

  getCount(topicId) {
    return this.dbMgr.stmts.getCount.get(topicId);
  }

  getTopics() {
    return this.dbMgr.stmts.getTopics.all();
  }

  getPostsAndTopics() {
    return this.dbMgr.stmts.getPostsAndTopics.all();
  }
}

module.exports = Db;
