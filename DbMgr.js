const SQLite = require("better-sqlite3");

class DbMgr {
  constructor(dbPath, tableSetups = [], sqlStmts = []) {
    this.db = new SQLite(dbPath);
    this.db.pragma("synchronous = 1");
    this.db.pragma("journal_mode = wal");
    this.db.pragma("foreign_keys = ON");
    this.stmts = {};

    tableSetups.forEach(({ tableName, tableFields }) => {
      if (!DbMgr.doesTableExist(this.db, tableName)) {
        const sql = `CREATE TABLE ${tableName} (${tableFields.join(", ")});`;
        this.db.prepare(sql).run();
      }
    });

    sqlStmts.forEach(({ name, sql }) => this.prepareStatement(name, sql));
  }

  prepareStatement(name, sql) {
    console.debug("preparing statement", name, sql);
    this.stmts[name] = this.db.prepare(sql);
  }

  static doesTableExist(database, tableName) {
    const sql = `SELECT count(*) FROM sqlite_master WHERE type='table' AND name = '${tableName}';`;
    const table = database.prepare(sql).get();
    return Boolean(table["count(*)"]);
  }
}

module.exports = DbMgr;
