A tiny forum-like api, without an ORM, in node.js, express, sqlite3


This is based off of some old code I wrote before I discovered ORM's, where I basically wrote my own (a very bad one).
- `crud.js` is a pseudo-ORM that I started writing before remembering I already wrote the one below, it's unused 
- `DbMgr.js` is a base that creates tables and prepares sql statements
- `TopicManager.js` uses the base above (and manages everything, misleading name). You'll notice that there's a lot of useless repetition for each of the methods that map to sql statements
- `routes-api.js` maps a lackluster REST API with the above "pseudo-ORM"
- `index.js` is just boilerplate code intializing the server
errors aren't handled etc


api usage:
```sh
#install and run the server
npm install
PORT=8881 node index.js


#post a new thread:
curl -d '{"title":"my cool topic", "author": "kevin", "content": "vive le site du zéro !"}' -H "Content-Type: application/json" -X POST http://localhost:8881/api/newtopic

#reply to a thread:
curl -d '{"threadId":"1", "author": "someone else", "content": "very true"}' -H "Content-Type: application/json" -X POST http://localhost:8881/api/reply

#delete a post:
curl -X DELETE localhost:8881/api/post/1

#delete a thread:
curl -X DELETE localhost:8881/api/topic/1

#get all topics
curl localhost:8881/api/topic

#get all posts, tied to their topics:
curl localhost:8881/api/getAll

#get all replies to a topic:
curl localhost:8881/api/replies/1
#paginated:
curl localhost:8881/api/replies/1/2
```
