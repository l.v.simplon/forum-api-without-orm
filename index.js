const server = require("express")();

const config = require("./config.js");
const routes = require("./routes-api.js");
const express = require("express");

const app = express();
app.use(express.json());
app.use(express.urlencoded({ extended: true }));

app.use("/api", routes);

app.listen(config.port, () => console.log("server listening"));
