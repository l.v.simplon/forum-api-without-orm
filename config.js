const config = {
  port: process.env.PORT || 8023,
  dbPath: "./forum.sqlite",
};

module.exports = config;
